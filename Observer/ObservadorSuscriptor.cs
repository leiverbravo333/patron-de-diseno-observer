﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    class ObservadorSuscriptor : IObservador
    {
        private string nombre;
        private SujetoForo sujeto;

        
        //Constructor para Suscriptor
        public ObservadorSuscriptor(string pNombre, SujetoForo pSujeto)
        {
            nombre = pNombre;
            sujeto = pSujeto;
            //La siguiente línea puede utilizarse para agregar al foro automáticamente al crear el suscriptor
            //sujeto.agregar(this);
        }

        //Método que se implementa para actalizar sobre cambios de estado
        public void Actualizar(string mensaje)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("'Se notifica a: {0}'- {1}", nombre, mensaje);
        }
    }
}
