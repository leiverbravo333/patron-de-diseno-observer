﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    //Se crea la interface para agregar los métodos que utilizará el observable para interactuar con los observadores
    interface ISujeto
    {
        //Permite agregar suscriptores al objeto observado
        void agregar(IObservador suscriptor);
        
        //Elimina suscriptores de la lista del objeto
        void eliminar(IObservador suscriptor);
        
        //Método para notificar los cambios de estado del sujeto
        void notificar();
    }
}
