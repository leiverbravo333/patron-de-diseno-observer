﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    //Se crea la interface para agregar el metodo que se encarga de actualizar a los suscriptores
    interface IObservador
    {
        //Permite recibir las notificaciones sobre cambios de estado
        void Actualizar(string mensaje);
    }
}
