﻿using System;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Se crea la instancia de SujetoForo, *el foro*
            SujetoForo Foro = new SujetoForo();

            //Se crean las instancias de ObservadorSuscriptor, *los suscriptores* y se los agrega al foro
            ObservadorSuscriptor suscriptorUno = new ObservadorSuscriptor("Matías", Foro);
            Foro.agregar(suscriptorUno);
            ObservadorSuscriptor suscriptorDos = new ObservadorSuscriptor("Mercedes", Foro);
            Foro.agregar(suscriptorDos);

            //Utilizamos un ciclo para cambiar de estado el observable varias veces
            for (int i = 0; i < 2; i++)
            {
                Foro.CambioEstado();
            }

            Console.WriteLine();
            //Se elimina al suscriptorDos
            Foro.eliminar(suscriptorDos);

            //Cambiamos de estado el observable varias veces y verificamos que no se notifica más el eliminado
            for (int i = 0; i < 2; i++)
            {
                Foro.CambioEstado();
            }

            Console.WriteLine();
            //Añadimos un nuevo suscriptor 
            ObservadorSuscriptor suscriptorTres = new ObservadorSuscriptor("Cecilia", Foro);
            Foro.agregar(suscriptorTres);

            //Cambiamos de estado el observable nuevamente y verificamos que se notifica de aquello
            Foro.CambioEstado();

        }
    }
}
