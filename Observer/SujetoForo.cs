﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    class SujetoForo : ISujeto
    {
        //Se crea una lista que almacena los observadores que se agreguen al sujeto observado
        private List<IObservador> suscriptores = new List<IObservador>();
        private string mensaje;


        //Se definen las funcionalidades de los métodos que implementa el Sujeto
        public void agregar(IObservador suscriptor)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Se agrega un nuevo suscriptor al foro");
            suscriptores.Add(suscriptor);
        }

        public void eliminar(IObservador suscriptor)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Se elimina un suscriptor del foro");
            suscriptores.Remove(suscriptor);
        }

        public void notificar()
        {
            //Se notifican los cambios de estado del foro a cada suscriptor
            foreach (IObservador observer in suscriptores)
            {
                observer.Actualizar(mensaje);
            }
        }

        //Método para realizar un cambio de estado que se notifique a los suscriptores
        public void CambioEstado()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("*****Se añade un artículo al foro*****");
            mensaje = "Se ha añadido un nuevo artículo, revise el foro para más detalles";
            notificar();
        }
    }
}
